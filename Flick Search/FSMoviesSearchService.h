//
//  FSMoviesSearchService.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSMoviesSearchService : NSObject

-(void)searchForMoviesWithString:(NSString *)searchString
                            page:(NSUInteger)page
               completionHandler:(void (^)(NSArray *, NSError *, NSUInteger))completion;

-(void)cancelSearch;

@end
