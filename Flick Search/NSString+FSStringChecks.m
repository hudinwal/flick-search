//
//  NSString+FSStringChecks.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "NSString+FSStringChecks.h"

@implementation NSString (FSStringChecks)

- (BOOL)stringHasCharacters
{
    NSCharacterSet* set = [NSCharacterSet whitespaceCharacterSet];
    if (!self || [[self stringByTrimmingCharactersInSet:set] length] == 0) {
        return NO;
    } else
        return YES;
}

- (BOOL)stringIsNotEmpty
{
    if (!self || [self isEqual:[NSNull null]] || [self length] == 0) {
        return NO;
    } else {
        return YES;
    }
}

+ (instancetype)stringPathForDirectory:(NSSearchPathDirectory)searchPath withSubPath:(NSString *)subPath
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(searchPath, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    if (subPath) {
        documentsDirectory = [NSString stringWithFormat:@"%@%@", documentsDirectory,subPath];
    }
    return documentsDirectory;
}

@end
