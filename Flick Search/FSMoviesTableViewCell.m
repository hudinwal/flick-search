//
//  FSMoviesTableViewCell.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSMoviesTableViewCell.h"
#import "NSString+FSStringChecks.h"
#import "UIImageView+RoundImageView.h"

@interface FSMoviesTableViewCell()

// For movie cell
@property (nonatomic,weak) IBOutlet UILabel * titleLabel;
@property (nonatomic,weak) IBOutlet UILabel * yearLabel;
@property (nonatomic,weak) IBOutlet UILabel * runningTimeLabel;
@property (nonatomic,weak) IBOutlet UILabel * ratingLabel;

// For recent cell
@property (nonatomic,weak) IBOutlet UILabel * queryLabel;
@property (nonatomic,weak) IBOutlet UILabel * recentNumberLabel;
@property (nonatomic,weak) IBOutlet UIView * viewToBeRounded;

@end

@implementation FSMoviesTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    UIImageView *iview=[UIImageView new];
    iview.backgroundColor = [UIColor colorWithWhite:0.149 alpha:1.000];
    self.selectedBackgroundView=iview;
}

-(void)setUpCellFormMovie:(FSMovie *)movie
{
    self.titleLabel.text = movie.title;
    
    if(movie.year>0)
        self.yearLabel.text = [@(movie.year) stringValue];
    else
        self.yearLabel.text = @"";
    
    if(movie.thumbnail) {
        self.thumbnailView.image = movie.thumbnail;
    }
    else
        self.thumbnailView.image = [UIImage imageNamed:@"SquareFilmStripBlack"];
    
    if(movie.runningTime>0)
        self.runningTimeLabel.text = [NSString stringWithFormat:@"%lu mins",(unsigned long)movie.runningTime];
    else
        self.runningTimeLabel.text = @"Unavailable";
    
    if(movie.audienceScore>0)
        self.ratingLabel.text = [NSString stringWithFormat:@"%lu / 100",(unsigned long)movie.audienceScore];
    else
        self.ratingLabel.text = @"Unavailable";
}

-(void)setUpCellForRecentQueryString:(NSString *)query
                          itemNumber:(NSUInteger)number
{
    [self.viewToBeRounded roundView];
    self.queryLabel.text = query;
    self.recentNumberLabel.text = @(number).stringValue;
}

@end
