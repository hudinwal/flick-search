//
//  NSURL+ApplicationURL.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "NSURL+ApplicationURL.h"
#import "FSDefines.h"

#define kRottenTomatoesKey @"c5eh5xsfheapyp5cued8qswa"
#define kBaseURL @"http://api.rottentomatoes.com/api/public/v1.0/"

@implementation NSURL (ApplicationURL)

+(instancetype) urlToSearchMoviesForQuery:(NSString *)query
                             atPageNumber:(NSUInteger)page
                           withItemsLimit:(NSUInteger)limit
{
    NSMutableString * urlString = [NSMutableString new];
    
    [urlString appendString:kBaseURL];
    [urlString appendString:@"movies.json?"];
    [urlString appendFormat:@"apikey=%@",kRottenTomatoesKey];
    
    [urlString appendFormat:@"&q=%@",[query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [urlString appendFormat:@"&page=%lu",(unsigned long)page];
    [urlString appendFormat:@"&page_limit=%lu",(unsigned long)(limit?limit:kDefaultPageLimit)];
    
    return [NSURL URLWithString:urlString];
}

+(instancetype)urlToFetchMovieDetailsForMovieID:(NSString *)movieID
{
    NSMutableString * urlString = [NSMutableString new];
    
    [urlString appendString:kBaseURL];
    [urlString appendFormat:@"movies/%@.json?",movieID];
    [urlString appendFormat:@"apikey=%@",kRottenTomatoesKey];
    
    return [NSURL URLWithString:urlString];
}

@end
