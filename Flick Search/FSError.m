//
//  FSError.m
//  Flick Search
//
//  Created by Dinesh on 26/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSError.h"
#import "NSString+FSStringChecks.h"

@implementation FSError

+(instancetype)errorWithCode:(NSInteger)code
                description:(NSString *)description
              failureReason:(NSString *)failureResaon
{
    return [[FSError alloc]initWithCode:code
                            description:description
                          failureReason:failureResaon];
}

-(instancetype)initWithCode:(NSInteger)code
                description:(NSString *)description
              failureReason:(NSString *)failureResaon
{
    NSMutableDictionary * info = [NSMutableDictionary new];
    if([description stringHasCharacters])
        [info setObject:description forKey:NSLocalizedDescriptionKey];
    if([failureResaon stringHasCharacters])
        [info setObject:failureResaon forKey:NSLocalizedFailureReasonErrorKey];

    self = [super initWithDomain:FSFlickSearchErrorDomain code:code userInfo:nil];
    return self;
}

@end

@implementation UIViewController (FSError)

-(void)handleError:(NSError *)error
{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:error.localizedDescription
                                                    message:error.localizedFailureReason
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

@end