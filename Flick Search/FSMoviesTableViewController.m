//
//  FSMoviesTableViewController.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSMoviesTableViewController.h"
#import "FSMoviesSearchService.h"
#import "FSDefines.h"
#import "FSError.h"
#import "FSLoadMoreView.h"
#import "FSMovie.h"
#import "FSMoviesTableViewCell.h"
#import "FSMovieDetailViewController.h"
#import "FSDatabaseManager.h"
#import "NSString+FSStringChecks.h"

@interface FSMoviesTableViewController () <UISearchBarDelegate>

{
    IBOutlet UISearchBar * searchBar;
    
    NSUInteger pageIndex;
    NSUInteger totalAvailableMoviesCount;
    NSIndexPath * lastCellIndexPath;
    FSFetchRequestType lastFetchType;
    NSMutableArray * fetchedMovies;
}

@property (nonatomic,strong) FSLoadMoreView * loadMoreView;
@property (nonatomic,strong) FSMoviesSearchService * searchService;

@end

@implementation FSMoviesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Restore animations that were paused
    [_loadMoreView restoreAnimation];
    
    // 
    if([_searchMoviesForString stringHasCharacters]){
        searchBar.text = _searchMoviesForString;
        [searchBar becomeFirstResponder];
        _searchMoviesForString = nil;
    }
}

//-----------------------------------------------------------------//
#pragma mark - External Methods
//-----------------------------------------------------------------//

-(void)setUpControllerToSearchString:(NSString *)queryString
{
}

//-----------------------------------------------------------------//
#pragma mark - Lazy Getters
//-----------------------------------------------------------------//

-(FSLoadMoreView *)loadMoreView
{
    if(!_loadMoreView)
        _loadMoreView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FSLoadMoreView class])
                                                       owner:self
                                                     options:nil]
                         firstObject];
    return _loadMoreView;
}

-(FSMoviesSearchService *)searchService
{
    if(!_searchService)
        _searchService = [FSMoviesSearchService new];
    return _searchService;
}

//-----------------------------------------------------------------//
#pragma mark - Search Request Handling
//-----------------------------------------------------------------//

-(void)fetchDataForCategory:(FSFetchRequestType)categoery
            forSearchString:(NSString *)searchString
{
    [_searchService cancelSearch];
    lastFetchType = categoery;
    
    // Update the UI to show progress
    [UIView animateWithDuration:0.0
                     animations:^{
                         self.tableView.tableFooterView = self.loadMoreView;
                     }];
    [self.loadMoreView startAnimating];
    
    if(categoery == FSFetchRequestTypeFresh) {
        pageIndex = 1;
        
        [self.searchService searchForMoviesWithString:searchString
                                                 page:pageIndex
                                    completionHandler:^(NSArray * movies, NSError * error, NSUInteger totalItems){
                                        [self updateViewForFetchedData:movies fetchError:error totalItems:totalItems];
                                    }];
    }
    else if(categoery == FSFetchRequestTypeLoadMore) {
        // Call service
        [self.searchService searchForMoviesWithString:searchString
                                                 page:pageIndex+1
                                    completionHandler:^(NSArray * movies, NSError * error, NSUInteger totalItems){
                                        [self updateViewForFetchedData:movies fetchError:error totalItems:totalItems];
                                    }];
    }
}

-(void)updateViewForFetchedData:(NSArray *)movies
                     fetchError:(NSError *)error
                     totalItems:(NSUInteger)totalItems
{
    _searchService = nil;
    // Stop the spining of the load more
    [self.loadMoreView stopAnimating];
    // Remove load more from the footer
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.tableView.tableFooterView = nil;
                     }];
    
    if(error) {
        [self handleError:error];
        return;
    }
    
    if(lastFetchType == FSFetchRequestTypeFresh)
    {
        // Add the search to recent searches
        NSError * insertError = nil;
        if(![[FSDatabaseManager sharedInstance] addSearchInRecentForString:searchBar.text
                                                                 error:&insertError])
            [self handleError:insertError];
        
        // Update instance variables from fetched data
        fetchedMovies = (NSMutableArray *)movies;
        totalAvailableMoviesCount = totalItems;
        [self.tableView reloadData];
    }
    else if(lastFetchType == FSFetchRequestTypeLoadMore)
    {
        pageIndex ++;
        // Reload table with new data
        [fetchedMovies addObjectsFromArray:movies];
        [self.tableView reloadData];
    }
}

//-----------------------------------------------------------------//
#pragma mark - Search Bar Delegate
//-----------------------------------------------------------------//

- (void)searchBarCancelButtonClicked:(UISearchBar *)sender
{
    sender.text = nil;
    [sender resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)sender
{
    if(sender.text)
        [self fetchDataForCategory:FSFetchRequestTypeFresh forSearchString:searchBar.text];
    [sender resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [fetchedMovies removeAllObjects];
    [self.tableView reloadData];
}


//-----------------------------------------------------------------//
#pragma mark - TableView Data Source and Delegate
//-----------------------------------------------------------------//

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    lastCellIndexPath = [NSIndexPath indexPathForRow:fetchedMovies.count-1 inSection:0];
    return fetchedMovies.count;
}

-(void)tableView:(UITableView *)tableView
 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(lastCellIndexPath.row == indexPath.row &&
       lastCellIndexPath.section == indexPath.section &&
       totalAvailableMoviesCount != indexPath.row+1)
    {
        if(pageIndex+1 <= ceilf(totalAvailableMoviesCount/25.0))
            [self fetchDataForCategory:FSFetchRequestTypeLoadMore forSearchString:searchBar.text];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FSMoviesTableViewCell * cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    FSMovie * mappedMovie = fetchedMovies[indexPath.row];
    [cell setUpCellFormMovie:mappedMovie];
    
    if (!mappedMovie.thumbnail && self.tableView.dragging == NO && self.tableView.decelerating == NO)
    {
        [self startThumbnailDownload:mappedMovie forIndexPath:indexPath];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.0;
}

#pragma mark - Table cell image support

- (void)startThumbnailDownload:(FSMovie *)movie forIndexPath:(NSIndexPath *)indexPath
{
    [movie fetchMovieThumbnailWithCompletionHandler:^(FSMovie * movieWithImage, NSError * error)
    {
        if(!error) {
            FSMoviesTableViewCell * cell = (FSMoviesTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            [UIView transitionWithView:cell.thumbnailView
                              duration:1.0f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                cell.thumbnailView.image = movie.thumbnail;
                            } completion:nil];
        }
    }];
}


- (void)loadImagesForOnscreenRows
{
    if (self)
    {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            FSMovie *movie = fetchedMovies[indexPath.row];
            
            if (!movie.thumbnail)
                // Avoid the app icon download if the app already has an icon
            {
                [self startThumbnailDownload:movie forIndexPath:indexPath];
            }
        }
    }
}


//-----------------------------------------------------------------//
#pragma mark - UIScrollViewDelegate
//-----------------------------------------------------------------//

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}

//-----------------------------------------------------------------//
#pragma mark - Seague
//-----------------------------------------------------------------//

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    FSMovieDetailViewController * destination;
    destination= (FSMovieDetailViewController *)segue.destinationViewController;
    NSIndexPath * selectedIndex = [self.tableView indexPathForSelectedRow];
    destination.movie = fetchedMovies[selectedIndex.row];
}

@end
