//
//  FSMovie.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FSMovie : NSObject

@property (nonatomic,strong,readonly) NSString * title;
@property (nonatomic,assign,readonly) NSUInteger year;
@property (nonatomic,assign,readonly) NSUInteger runningTime;
@property (nonatomic,assign,readonly) NSUInteger audienceScore;
@property (nonatomic,strong,readonly) NSString * movieId;
@property (nonatomic,strong,readonly) UIImage * thumbnail;

// Detailed Information
@property (nonatomic,strong,readonly) UIImage *poster;
@property (nonatomic,strong,readonly) NSString * synopsis;

-(instancetype)initWithInfoDictionary:(NSDictionary *)info;

-(void)fetchMovieThumbnailWithCompletionHandler:(void (^)(FSMovie *, NSError *))completion;
-(void)fetchMoviePosterWithCompletionHandler:(void (^)(FSMovie *,NSError *))completion;
-(void)fetchMovieDetailsWithCompletionHandler:(void (^)(FSMovie *, NSError *))completion;

@end
