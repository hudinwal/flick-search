//
//  FSWebInterface.h
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSWebInterface : NSObject

@property (nonatomic, readonly, strong) NSError * webInterfaceError;
@property (nonatomic, readonly, assign) BOOL inProgress;

-(void)createConnectionWithUrl:(NSURL*)url
             completionHandler:(void(^)(NSData *,NSError *))completionHandler;
-(void)cancelConnection;

@end
