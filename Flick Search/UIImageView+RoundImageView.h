//
//  UIImageView+RoundImageView.h
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RoundView)

-(void)roundView;

@end
