//
//  FSMoviesSearchService.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSMoviesSearchService.h"
#import "FSWebInterface.h"
#import "NSURL+ApplicationURL.h"
#import "NSDictionary+JsonDictionary.h"
#import "FSMovie.h"
#import "FSError.h"

@interface FSMoviesSearchService()

@property (nonatomic, strong) FSWebInterface * searchWebInterface;
@property (nonatomic, copy) void (^completionHandler)(NSArray *, NSError *, NSUInteger);

@end

@implementation FSMoviesSearchService

-(void)searchForMoviesWithString:(NSString *)searchString
                            page:(NSUInteger)page
               completionHandler:(void (^)(NSArray *, NSError *, NSUInteger))completion
{
    _completionHandler = completion;
    _searchWebInterface = [FSWebInterface new];
    
    NSURL * searchURL = [NSURL urlToSearchMoviesForQuery:searchString
                                            atPageNumber:page
                                          withItemsLimit:0];
    
    [_searchWebInterface createConnectionWithUrl:searchURL
                               completionHandler:^(NSData * data,NSError * error)
    {
        _searchWebInterface = nil;
        [self handleServerData:data error:error];
    }];
    
    
}

-(void)cancelSearch
{
	if(_searchWebInterface)
       [_searchWebInterface cancelConnection];
    _searchWebInterface = nil;
    _completionHandler = NULL;
}

-(void)handleServerData:(NSData *)data error:(NSError *) webInterfaceError
{
    // Sanity Check
    if(_completionHandler == NULL) return;
    
    if(webInterfaceError) {
        _completionHandler(nil,webInterfaceError,0);
        return;
    }
    NSError * error = nil;
    NSDictionary * moviesInfo = [NSJSONSerialization JSONObjectWithData:data
                                                                options:kNilOptions
                                                                  error:&error];
    if(moviesInfo) {
        if([moviesInfo nonNullObjectForKey:@"error"])
        {
            NSError * apiError = [FSError errorWithCode:0
                                            description:@"Request Failed at Server"
                                          failureReason:[moviesInfo nonNullObjectForKey:@"error"]];
            _completionHandler(nil,apiError,0);
        }
        else {
            NSArray * moviesInfoList = [moviesInfo nonNullObjectForKey:@"movies"];
            NSUInteger totalItems = [moviesInfo unsignedIntegerForKey:@"total"];
            NSMutableArray * moviesList = [NSMutableArray new];
            for (NSDictionary * movieInfo in moviesInfoList) {
                FSMovie * movie = [[FSMovie alloc]initWithInfoDictionary:movieInfo];
                [moviesList addObject:movie];
            }
            _completionHandler(moviesList,nil,totalItems);
        }
    }
    else
        _completionHandler(nil,error,0);
}

@end
