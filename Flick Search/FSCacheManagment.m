//
//  FSCacheManagment.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSCacheManagment.h"
#import "NSString+FSStringChecks.h"

@interface FSCacheManagment()

@property (nonatomic,strong) NSFileManager * defaultFileManager;
@property (nonatomic,strong) NSString * imagesCacheDirectoryPath;
@property (nonatomic,strong) NSString * thumbnailCacheDirectoryPath;
@property (nonatomic,strong) NSString * posterCacheDirectoryPath;

@end

@implementation FSCacheManagment

@dynamic imagesCacheDirectoryPath,thumbnailCacheDirectoryPath,posterCacheDirectoryPath;

static FSCacheManagment * sharedInstance = nil;

+ (instancetype)sharedInstance
{
    static dispatch_once_t pred;
    
    if (nil != sharedInstance)
        return sharedInstance;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[FSCacheManagment alloc] init];
    });
    
    return sharedInstance;
}

-(instancetype)init
{
    if(self = [super init])
    {
        _defaultFileManager          = [NSFileManager defaultManager];
    }
    return self;
}

-(void)clearImagesCache
{
    if ([_defaultFileManager fileExistsAtPath:[NSString stringPathForDirectory:NSCachesDirectory
                                                                   withSubPath:@"/images"]]) {
        [_defaultFileManager removeItemAtPath:[NSString stringPathForDirectory:NSCachesDirectory
                                                                   withSubPath:@"/images"] error:nil];
    }

}

//-----------------------------------------------------------------//
#pragma mark - Fetch Paths
//-----------------------------------------------------------------//

-(NSString *)imagesCacheDirectoryPath
{
    NSString* imagesCacheDirectory = [NSString stringPathForDirectory:NSCachesDirectory
                                                          withSubPath:@"/images/"];
    
    if (![_defaultFileManager fileExistsAtPath:imagesCacheDirectory]) {
        [_defaultFileManager createDirectoryAtPath:imagesCacheDirectory
            withIntermediateDirectories:YES
                             attributes:nil
                                  error:nil];
    }
    return imagesCacheDirectory;
}

-(NSString *)thumbnailCacheDirectoryPath
{
    NSString* thumbnailCacheDirectory = [NSString stringPathForDirectory:NSCachesDirectory
                                                             withSubPath:@"/images/thumbnails/"];
    
    if (![_defaultFileManager fileExistsAtPath:thumbnailCacheDirectory]) {
        [_defaultFileManager createDirectoryAtPath:thumbnailCacheDirectory
                       withIntermediateDirectories:YES
                                        attributes:nil
                                             error:nil];
    }
    return thumbnailCacheDirectory;
}

-(NSString *)posterCacheDirectoryPath
{
    NSString* posterCacheDirectory = [NSString stringPathForDirectory:NSCachesDirectory
                                                          withSubPath:@"/images/posters/"];
    
    if (![_defaultFileManager fileExistsAtPath:posterCacheDirectory]) {
        [_defaultFileManager createDirectoryAtPath:posterCacheDirectory
                       withIntermediateDirectories:YES
                                        attributes:nil
                                             error:nil];
    }
    return posterCacheDirectory;
}

//-----------------------------------------------------------------//
#pragma mark - Thumbnail Cache
//-----------------------------------------------------------------//

- (void)cacheMovieThumbnailData:(NSData *)imageData
                     forMovieID:(NSString *)movieID
{
    if(!imageData) return;
    
    NSString * imagePath = [NSString stringWithFormat:@"%@%@",self.thumbnailCacheDirectoryPath,movieID];
    [imageData writeToFile:imagePath atomically:YES];
}

- (UIImage *)cachedMovieThumbnailForMovieID:(NSString *)movieID
{
    NSString * imagePath = [NSString stringWithFormat:@"%@%@",self.thumbnailCacheDirectoryPath,movieID];
    if([_defaultFileManager fileExistsAtPath:imagePath])
        return [UIImage imageWithContentsOfFile:imagePath];
    else
        return nil;
}

//-----------------------------------------------------------------//
#pragma mark - Poster Cache
//-----------------------------------------------------------------//

- (void)cacheMoviePosterData:(NSData *)imageData
                  forMovieID:(NSString *)movieID
{
    if(!imageData) return;
    
    NSString * imagePath = [NSString stringWithFormat:@"%@%@",self.posterCacheDirectoryPath,movieID];
    [imageData writeToFile:imagePath atomically:YES];
}

- (UIImage *)cachedMoviePosterForMovieID:(NSString *)movieID
{
    NSString * imagePath = [NSString stringWithFormat:@"%@%@",self.posterCacheDirectoryPath,movieID];
    if([_defaultFileManager fileExistsAtPath:imagePath])
        return [UIImage imageWithContentsOfFile:imagePath];
    else
        return nil;
}





@end
