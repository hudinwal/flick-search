//
//  NSDictionary+JsonDictionary.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JsonDictionary)

-(id)nonNullObjectForKey:(id)aKey;
-(NSUInteger)unsignedIntegerForKey:(id)aKey;
-(NSInteger)integerForKey:(id)aKey;

@end
