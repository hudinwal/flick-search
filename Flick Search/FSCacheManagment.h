//
//  FSCacheManagment.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FSCacheManagment : NSObject

+ (instancetype)sharedInstance;

- (void)clearImagesCache;

//-----------------------------------------------------------------//
#pragma mark - Thumbnail Cache
//-----------------------------------------------------------------//

- (void)cacheMovieThumbnailData:(NSData *)imageData
                  forMovieID:(NSString *)movieID;
- (UIImage *)cachedMovieThumbnailForMovieID:(NSString *)movieID;


//-----------------------------------------------------------------//
#pragma mark - Poster Cache
//-----------------------------------------------------------------//

- (void)cacheMoviePosterData:(NSData *)imageData
                  forMovieID:(NSString *)movieID;
- (UIImage *)cachedMoviePosterForMovieID:(NSString *)movieID;
@end
