//
//  FSDatabaseManager.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecentSearch.h"
#import <CoreData/CoreData.h>

@interface FSDatabaseManager : NSObject

+ (instancetype)sharedInstance;
- (BOOL)saveContextWithError:(NSError **)error;

//-----------------------------------------------------------------//
#pragma mark - Search Table
//-----------------------------------------------------------------//

// Defaults to 10
@property (nonatomic,assign) NSUInteger maxSearchesInRecent;
// Fetches all recent searches
-(NSArray *)recentSearches:(NSError **)error;
// Adds item to recent searches
-(BOOL)addSearchInRecentForString:(NSString *)searchString error:(NSError **)error;

@end
