//
//  FSNetworkActivity.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSNetworkActivity : NSObject

+(instancetype)sharedInstance;
+(void)start;
+(void)finished;

@end
