//
//  FSError.h
//  Flick Search
//
//  Created by Dinesh on 26/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define FSFlickSearchErrorDomain @"FSFlickSearchErrorDomain"

@interface FSError : NSError

+(instancetype)errorWithCode:(NSInteger)code
                description:(NSString *)description
              failureReason:(NSString *)failureResaon;

-(instancetype)initWithCode:(NSInteger)code
                description:(NSString *)description
              failureReason:(NSString *)failureResaon;
@end

@interface UIViewController (FSError)

-(void)handleError:(NSError *)error;

@end
