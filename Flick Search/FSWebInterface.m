//
//  FSWebInterface.m
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSWebInterface.h"
// This framework was imported so we could use the kCFURLErrorNotConnectedToInternet error code.
#import <CFNetwork/CFNetwork.h>
#import "FSError.h"
#import <UIKit/UIKit.h>
#import "FSNetworkActivity.h"

@interface FSWebInterface ()

@property (nonatomic, strong) NSError * webInterfaceError;
@property (nonatomic, strong) NSURLConnection *webConnection;
@property (nonatomic, strong) NSMutableData *fetchedData;
@property (nonatomic, assign) BOOL inProgress;

/// A block to call when data is fetched.
@property (nonatomic, copy) void (^completionHandler)(NSData * data, NSError *error);

@end

@implementation FSWebInterface

-(id)init
{
    if(self = [super init]){
        
    }
    return self;
}
#pragma mark -

// -------------------------------------------------------------------------------
//	createConnectionWithUrl:url
// -------------------------------------------------------------------------------
-(void)createConnectionWithUrl:(NSURL*)url
             completionHandler:(void (^)(NSData *, NSError *))completionHandler
{
    _completionHandler = completionHandler;
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    _webConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    // Test the validity of the connection object. The most likely reason for the connection object
    // to be nil is a malformed URL, which is a programmatic error easily detected during development
    // If the URL is more dynamic, then you should implement a more flexible validation technique, and
    // be able to both recover from errors and communicate problems to the user in an unobtrusive manner.
    //
    NSAssert(_webConnection != nil, @"Failure to create URL connection.");
    
    
    _inProgress = YES;
    // show in the status bar that network activity is starting
    [FSNetworkActivity start];
}
-(void)cancelConnection
{
    if(_inProgress && _webConnection) {
        [_webConnection cancel];
        _inProgress = NO;
        [FSNetworkActivity finished];
    }
    _completionHandler = NULL;
    _webConnection = nil;
    _fetchedData = nil;
    _webInterfaceError = nil;
}

// -------------------------------------------------------------------------------
//	handleError:error
// -------------------------------------------------------------------------------
- (void)handleError:(NSError *)error
{
    if (_completionHandler) {
        _completionHandler(nil,error);
    }
}

// The following are delegate methods for NSURLConnection. Similar to callback functions, this is how
// the connection object,  which is working in the background, can asynchronously communicate back to
// its delegate on the thread from which it was started - in this case, the main thread.
//
#pragma mark - NSURLConnectionDelegate methods

// -------------------------------------------------------------------------------
//	connection:didReceiveResponse:response
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSInteger responseStatusCode = [((NSHTTPURLResponse *)response) statusCode];
    
    NSString * errorReason = [NSHTTPURLResponse localizedStringForStatusCode:responseStatusCode ];
    if (responseStatusCode ==200) {
        
        _fetchedData = [NSMutableData data];    // start off with new data
    }
    else{
        /// Server responded with HTTP error
        // Creating error object
        NSError * error = [FSError errorWithCode:0
                                     description:@"HTTP Connection Error"
                                   failureReason:errorReason];
        // Set error property
        _webInterfaceError = error;
        [self handleError:error];
    }
}

// -------------------------------------------------------------------------------
//	connection:didReceiveData:data
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_fetchedData appendData:data];  // append incoming data
}

// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    _inProgress = NO;
    [FSNetworkActivity finished];
    
    if ([error code] == kCFURLErrorNotConnectedToInternet)
	{
        // if we can identify the error, we can present a more precise message to the user.
        NSDictionary * userInfo = @{NSLocalizedDescriptionKey:@"No Connection Error",
                                    NSLocalizedFailureReasonErrorKey:@"Please check the internet connection."};
        NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain
														 code:kCFURLErrorNotConnectedToInternet
													 userInfo:userInfo];
        [self handleError:noConnectionError];
    }
	else
	{
        // otherwise handle the error generically
        [self handleError:error];
    }
    
    _webConnection = nil;   // release our connection
}

// -------------------------------------------------------------------------------
//	connectionDidFinishLoading:connection
// -------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _inProgress = NO;
    [FSNetworkActivity finished];
    
    _webConnection = nil;   // release our connection
    
    // return the fetched data only in case there was no error
    if(_completionHandler && !_webInterfaceError){
        _completionHandler(_fetchedData,nil);
    }
}

@end
