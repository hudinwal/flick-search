//
//  FSMovieDetailViewController.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSMovieDetailViewController.h"
#import "UIImageView+RoundImageView.h"
#import "FSError.h"
#import "NSString+FSStringChecks.h"

@interface FSMovieDetailViewController ()
{
    IBOutlet UIImageView * posterImageView;
    IBOutlet UILabel * titleLabel;
    IBOutlet UILabel * yearLabel;
    IBOutlet UILabel * runningTimeLabel;
    IBOutlet UILabel * ratingLabel;
    IBOutlet UILabel * synopsisLabel;
    
    IBOutlet UIActivityIndicatorView * detailsFetchSpinner;
    IBOutlet UIActivityIndicatorView * posterFetchSpinner;
    
}
@end

@implementation FSMovieDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpView];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(!_movie.synopsis) {
        [detailsFetchSpinner startAnimating];
        [_movie fetchMovieDetailsWithCompletionHandler:^(FSMovie * movie, NSError * error){
            [detailsFetchSpinner stopAnimating];
            if(error) [self handleError:error];
            else {
                if([movie.synopsis stringHasCharacters]) synopsisLabel.text = movie.synopsis;
                else synopsisLabel.text = @"Synopsis not available";
            }
        }];
    }
    if(!_movie.poster) {
        [posterFetchSpinner startAnimating];
        [_movie fetchMoviePosterWithCompletionHandler:^(FSMovie * movie, NSError * error){
            [posterFetchSpinner stopAnimating];
            if(!error) {
                 [UIView transitionWithView:posterImageView
                              duration:1.0f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                posterImageView.image = movie.poster;
                            } completion:nil];
                posterImageView.image = movie.poster;
            }
                
        }];
    
    }
}

-(void)setUpView
{
    self.navigationItem.title = _movie.title;
    titleLabel.text = _movie.title;
    yearLabel.text = @(_movie.year).stringValue;
    
    if(_movie.runningTime>0)
        runningTimeLabel.text = [NSString stringWithFormat:@"%lu mins",(unsigned long)_movie.runningTime];
    else
        runningTimeLabel.text = @"Unavailable";
    
    if(_movie.audienceScore>0)
        ratingLabel.text = [NSString stringWithFormat:@"%lu / 100",(unsigned long)_movie.audienceScore];
    else
        ratingLabel.text = @"Unavailable";


    if(_movie.synopsis)
        synopsisLabel.text = _movie.synopsis;
    else
        synopsisLabel.text = @"";
    if(_movie.poster)
        posterImageView.image = _movie.poster;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    [posterImageView roundView];
}

@end
