//
//  FSRecentSearchTableViewController.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSRecentSearchTableViewController.h"
#import "FSDatabaseManager.h"
#import "FSError.h"
#import "FSMoviesTableViewController.h"
#import "FSMoviesTableViewCell.h"

@interface FSRecentSearchTableViewController ()
{
    NSArray * recentSearches;
}
@end

@implementation FSRecentSearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = 44.0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSError * error = nil;
    recentSearches = [[FSDatabaseManager sharedInstance] recentSearches:&error];
    if(!recentSearches) [self handleError:error];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-----------------------------------------------------------------//
#pragma mark - Table view data source
//-----------------------------------------------------------------//

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return recentSearches.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecentSearch * recentSearch = recentSearches[indexPath.row];
    FSMoviesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell_ID"];
    [cell setUpCellForRecentQueryString:recentSearch.queryString itemNumber:indexPath.row+1];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecentSearch * recentSearch = recentSearches[indexPath.row];
    [self jumpToSeachScreenWithQueryString:recentSearch.queryString];
}

-(void)jumpToSeachScreenWithQueryString:(NSString *)queryString
{
    UINavigationController * parentController = self.tabBarController.viewControllers[0];
    FSMoviesTableViewController * searchController = parentController.viewControllers[0];
    searchController.searchMoviesForString = queryString;
    [parentController popToRootViewControllerAnimated:NO];
    self.tabBarController.selectedIndex = 0;
}
@end
