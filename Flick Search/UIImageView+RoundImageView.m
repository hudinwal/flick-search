//
//  UIImageView+RoundImageView.m
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIImageView+RoundImageView.h"

@implementation UIView (RoundView)

-(void)roundView
{
    //self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = (self.frame.size.width / 2);
    self.layer.masksToBounds = YES;
    //self.layer.borderWidth = 0.5;
    //self.layer.borderColor = [UIColor blackColor].CGColor;
}
@end
