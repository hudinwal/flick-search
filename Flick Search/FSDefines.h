//
//  FSDefines.h
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#ifndef Flick_Search_FSDefines_h
#define Flick_Search_FSDefines_h

//-----------------------------------------------------------------//
#pragma mark - Defaults Constants Defines
//-----------------------------------------------------------------//

#define kDefaultPageLimit 30

typedef NS_ENUM(NSInteger, FSFetchRequestType)
{
    FSFetchRequestTypeFresh,
    FSFetchRequestTypeLoadMore,
};

//-----------------------------------------------------------------//
#pragma mark - Loggers
//-----------------------------------------------------------------//

    #ifdef DEBUG
        #define DLog(...) NSLog(@"%s(%p) %@", __PRETTY_FUNCTION__, self, [NSString stringWithFormat:__VA_ARGS__])
        #define ALog(...)\
        {\
            NSLog(@"%s(%p) %@", __PRETTY_FUNCTION__, self,[NSString stringWithFormat:__VA_ARGS__]);\
            [[NSAssertionHandler currentHandler] handleFailureInFunction:[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] file:[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding]lineNumber:__LINE__ description:__VA_ARGS__];\
        }
    #else
        #define DLog(...) do { } while (0)

    #ifndef NS_BLOCK_ASSERTIONS
        #define NS_BLOCK_ASSERTIONS
    #endif

    #define ALog(...) NSLog(@"%s(%p) %@", __PRETTY_FUNCTION__, self, \ [NSString stringWithFormat:__VA_ARGS__])
    #endif

    #define ZAssert(condition, ...) do { \ if (!(condition)) { \ ALog(__VA_ARGS__); \ }\ } while(0)

#endif