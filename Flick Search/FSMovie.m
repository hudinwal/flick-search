//
//  FSMovie.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSMovie.h"
#import "NSDictionary+JsonDictionary.h"
#import "FSCacheManagment.h"
#import "FSWebInterface.h"
#import "NSURL+ApplicationURL.h"

#define kTitleKey           @"title"
#define kYearKey            @"year"
#define kLinksKey           @"links"
#define kPostersKey         @"posters"
#define kThumbnailURLKey    @"thumbnail"
#define kPosterURLKey       @"original"
#define kSynopsisKey        @"synopsis"
#define kIDKey              @"id"
#define kDetailedInfoURLKey @"self"
#define kRatingKey          @"ratings"
#define kAudienceScoreKey   @"audience_score"
#define kRunningTimeKey     @"runtime"

@interface FSMovie()

@property (nonatomic, strong) NSString * title;
@property (nonatomic, assign) NSUInteger year;
@property (nonatomic, strong) NSString * thumbnailURLString;
@property (nonatomic, strong) NSString * posterURLString;
@property (nonatomic, assign) NSUInteger runningTime;
@property (nonatomic, assign) NSUInteger audienceScore;

@property (nonatomic, strong) NSString * movieId;
@property (nonatomic, strong) NSString * synopsis;


@property (nonatomic,strong) FSWebInterface * thumbnailWebInterface;
@property (nonatomic,strong) FSWebInterface * posterWebInterface;
@property (nonatomic,strong) FSWebInterface * detailsWebInterface;

@property (nonatomic, copy) void (^thumbnailFetchCompletion)(FSMovie *, NSError * );
@property (nonatomic, copy) void (^posterFetchCompletion)(FSMovie *, NSError * );
@property (nonatomic, copy) void (^detailFetchCompletion)(FSMovie *, NSError * );

@end

@implementation FSMovie

@dynamic thumbnail;
@dynamic poster;

-(instancetype)initWithInfoDictionary:(NSDictionary *)info
{
    if(self = [super init]) {
        
        _title = [info nonNullObjectForKey:kTitleKey];
        _year = [info unsignedIntegerForKey:kYearKey];
        _runningTime = [info unsignedIntegerForKey:kRunningTimeKey];
        
        NSDictionary * postersInfo = [info nonNullObjectForKey:kPostersKey];
        _thumbnailURLString = [postersInfo nonNullObjectForKey:kThumbnailURLKey];
        _posterURLString = [postersInfo nonNullObjectForKey:kPosterURLKey];
        
        NSDictionary * ratingsInfo = [info nonNullObjectForKey:kRatingKey];
        _audienceScore = [ratingsInfo unsignedIntegerForKey:kAudienceScoreKey];

        _movieId = [info nonNullObjectForKey:kIDKey];
    }
    return self;
}

-(UIImage *)thumbnail
{
    return [[FSCacheManagment sharedInstance] cachedMovieThumbnailForMovieID:_movieId];
}

-(UIImage *)poster
{
    return [[FSCacheManagment sharedInstance] cachedMoviePosterForMovieID:_movieId];
}

//-----------------------------------------------------------------//
#pragma mark - Thumbnail Fetching
//-----------------------------------------------------------------//

-(void)fetchMovieThumbnailWithCompletionHandler:(void (^)(FSMovie * movie, NSError * error))completion
{
    _thumbnailFetchCompletion = completion;
    _thumbnailWebInterface = [FSWebInterface new];
    NSURL * thumbnailURL = [NSURL URLWithString:_thumbnailURLString];
    [_thumbnailWebInterface createConnectionWithUrl:thumbnailURL
                                 completionHandler:^(NSData * imageData, NSError * error)
    {
        [self thumbnailFetchCompleteWithData:imageData error:error];
    }];
}

-(void)thumbnailFetchCompleteWithData:(NSData *)data error:(NSError *)error
{
    _thumbnailWebInterface = nil;
    if(!error) {
        [[FSCacheManagment sharedInstance] cacheMovieThumbnailData:data
                                                        forMovieID:_movieId];
        data = nil;
        if(_thumbnailFetchCompletion != NULL) {
            _thumbnailFetchCompletion(self,nil);
            _thumbnailFetchCompletion = NULL;
        }
    }
    else
        if(_thumbnailFetchCompletion != NULL) {
            _thumbnailFetchCompletion(self,error);
            _thumbnailFetchCompletion = NULL;
        }
}

//-----------------------------------------------------------------//
#pragma mark - Details Fetching
//-----------------------------------------------------------------//

-(void)fetchMovieDetailsWithCompletionHandler:(void (^)(FSMovie * movie, NSError * error))completion
{
    _detailFetchCompletion = completion;
    _detailsWebInterface = [FSWebInterface new];
    NSURL * detailsURL = [NSURL urlToFetchMovieDetailsForMovieID:_movieId];
    [_detailsWebInterface createConnectionWithUrl:detailsURL
                                 completionHandler:^(NSData * detailsData, NSError * error)
     {
         [self detailsFetchCompleteWithData:detailsData error:error];
     }];
}

-(void)detailsFetchCompleteWithData:(NSData *)data error:(NSError *)error
{
    _detailsWebInterface = nil;
    if(!error) {
        [self parseFetchedDetailsData:data];
    }
    else
        if(_detailFetchCompletion != NULL) {
            _detailFetchCompletion(self,error);
            _detailFetchCompletion = NULL;
        }
}

-(void)parseFetchedDetailsData:(NSData *)jsonData
{
    // Sanity check
    if(_detailFetchCompletion == NULL) return;
    
    NSError * parseError;
    NSDictionary * detailsInfo = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                 options:kNilOptions
                                                                   error:&parseError];
    if(detailsInfo) {
        _synopsis = [detailsInfo nonNullObjectForKey:kSynopsisKey];
        _detailFetchCompletion(self,nil);
        _detailFetchCompletion = NULL;
    }
    else {
        _detailFetchCompletion(self,parseError);
        _detailFetchCompletion = NULL;
    }
}

//-----------------------------------------------------------------//
#pragma mark - Poster Fetching
//-----------------------------------------------------------------//

-(void)fetchMoviePosterWithCompletionHandler:(void (^)(FSMovie * movie, NSError * error))completion
{
    _posterFetchCompletion = completion;
    _posterWebInterface = [FSWebInterface new];
    NSURL * posterURL = [NSURL URLWithString:_posterURLString];
    [_posterWebInterface createConnectionWithUrl:posterURL
                                  completionHandler:^(NSData * imageData, NSError * error)
     {
         [self posterFetchCompleteWithData:imageData error:error];
     }];
}

-(void)posterFetchCompleteWithData:(NSData *)data error:(NSError *)error
{
    _posterWebInterface = nil;
    if(!error) {
        [[FSCacheManagment sharedInstance] cacheMoviePosterData:data
                                                     forMovieID:_movieId];
        data = nil;
        if(_posterFetchCompletion != NULL) {
            _posterFetchCompletion(self,nil);
            _posterFetchCompletion = NULL;
        }
    }
    else
        if(_posterFetchCompletion != NULL) {
            _posterFetchCompletion(self,error);
            _posterFetchCompletion = NULL;
        }
}

@end
