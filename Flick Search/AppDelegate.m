//
//  AppDelegate.m
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "FSDatabaseManager.h"
#import "FSCacheManagment.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setUpAppearanceProxy];
    [[FSCacheManagment sharedInstance] clearImagesCache];
    [FSDatabaseManager sharedInstance];
    return YES;
}

//-----------------------------------------------------------------//
#pragma mark - Appeareance Proxy
//-----------------------------------------------------------------//


-(void)setUpAppearanceProxy
{
    [[UIApplication sharedApplication] delegate].window.tintColor = [UIColor colorWithRed:0.953 green:0.349 blue:0.224 alpha:1.000];
    [UINavigationBar appearance].barTintColor = [UIColor blackColor];
    [UINavigationBar appearance].titleTextAttributes = @{ NSForegroundColorAttributeName:[UIColor colorWithRed:0.953 green:0.349 blue:0.224 alpha:1.000]};
    [UITabBar appearance].barTintColor = [UIColor blackColor];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor colorWithWhite:0.149 alpha:1.000]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor colorWithWhite:0.800 alpha:1.000]];
    [[UISearchBar appearance] setTintColor:[UIColor colorWithWhite:0.800 alpha:1.000]];
}



@end
