//
//  FSLoadMoreView.h
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSLoadMoreView : UIView

/**
 Property to know if the view is animating or not.
 */
@property (nonatomic,assign,readonly) BOOL isAnimating;

/**
 Starts the spin animations
 */
-(void)startAnimating;

/**
 Stops the spining animations. Does not removes the load more view.
 Handle the removal of the load more view in the implemetation view
 controller.
 */
-(void)stopAnimating;

/**
 Restart any paused animations
 */
-(void)restoreAnimation;

@end
