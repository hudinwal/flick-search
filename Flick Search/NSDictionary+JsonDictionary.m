//
//  NSDictionary+JsonDictionary.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "NSDictionary+JsonDictionary.h"
#import <Foundation/Foundation.h>

@implementation NSDictionary (JsonDictionary)

-(id)nonNullObjectForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if(object == [NSNull null])
        return nil;
    else
        return object;
}

-(NSUInteger)unsignedIntegerForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if([object isKindOfClass:[NSNumber class]] )
        return [(NSNumber*)object unsignedIntegerValue];
    else if([object isKindOfClass:[NSString class]] )
        return [(NSString*)object integerValue];
    else
        return 0;

}

-(NSInteger)integerForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    if([object isKindOfClass:[NSNumber class]] )
        return [(NSNumber*)object integerValue];
    else if([object isKindOfClass:[NSString class]] )
        return [(NSString*)object integerValue];
    else
        return 0;
}

@end

