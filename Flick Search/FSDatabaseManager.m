//
//  FSDatabaseManager.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSDatabaseManager.h"
#import <UIKit/UIKit.h>
#import "FSError.h"
#import "RecentSearch.h"

@interface FSDatabaseManager()

@property (strong, nonatomic) NSManagedObjectContext       *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel         *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation FSDatabaseManager

//-----------------------------------------------------------------//
#pragma mark - Core Data Setup
//-----------------------------------------------------------------//

static FSDatabaseManager * sharedInstance = nil;

+ (instancetype)sharedInstance {
    
    static dispatch_once_t pred;
    
    if (nil != sharedInstance)
        return sharedInstance;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[FSDatabaseManager alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        _maxSearchesInRecent = 10;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(saveContextWithError:)
                                                     name:UIApplicationWillTerminateNotification
                                                   object:nil];
    }
    return self;
}

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillTerminateNotification
                                                  object:nil];
}

- (NSURL *)applicationDocumentsDirectory {
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Flick_Search"
                                              withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Flick_Search.sqlite"];
    
    [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                              configuration:nil
                                                        URL:storeURL
                                                    options:nil
                                                      error:nil];
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (BOOL)saveContextWithError:(NSError *__autoreleasing *)error {
    
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges])
        {
            if(![managedObjectContext save:error])
                return NO;
        }
        return YES;
    }
    else
    {
        if(error != NULL)
        *error = [FSError errorWithCode:0
                             description:@"Database Error"
                           failureReason:@"Unable to setup connectivity to database."];
        return NO;
    }
}

//-----------------------------------------------------------------//
#pragma mark - Search Table
//-----------------------------------------------------------------//

-(NSUInteger)getRecentSearchCount:(NSError **)error
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([RecentSearch class])
                                   inManagedObjectContext:self.managedObjectContext]];
    [request setIncludesSubentities:NO];
    return [self.managedObjectContext countForFetchRequest:request error:error];
}

-(BOOL)deleteOldestSearch:(NSError **)error
{
    NSArray * recentSearches = [self recentSearches:error];
    if(!recentSearches) {
        return NO;
    }
    else {
        [self.managedObjectContext deleteObject:recentSearches.lastObject];
        return [self saveContextWithError:error];
    }
}

-(NSArray *)recentSearches:(NSError **)error
{
    NSFetchRequest * allRecentSearchFetcher = [[NSFetchRequest alloc]init];
    NSString * entityName = NSStringFromClass([RecentSearch class]);
    [allRecentSearchFetcher setEntity:[NSEntityDescription entityForName:entityName
                                                  inManagedObjectContext:self.managedObjectContext]];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO];
    [allRecentSearchFetcher  setSortDescriptors:@[sort]];
    
    return [self.managedObjectContext executeFetchRequest:allRecentSearchFetcher
                                                    error:error];
    
}

-(RecentSearch *)recentSearchWithQueryString:(NSString *)string
                                       error:(NSError **)error
{
    NSFetchRequest * recentSearchFetcher = [[NSFetchRequest alloc]init];
    NSString * entityName = NSStringFromClass([RecentSearch class]);
    [recentSearchFetcher setEntity:[NSEntityDescription entityForName:entityName
                                                  inManagedObjectContext:self.managedObjectContext]];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"queryString like[cd] %@", string];
    [recentSearchFetcher setPredicate:predicate];
    
    return [[self.managedObjectContext executeFetchRequest:recentSearchFetcher
                                                    error:error] firstObject];
}

-(BOOL)addSearchInRecentForString:(NSString *)searchString
                            error:(NSError **)error
{
    // If search already in table update its timestamp and return.
    RecentSearch * alreadyExisting = [self recentSearchWithQueryString:searchString
                                                                 error:error];
    if(alreadyExisting) {
        alreadyExisting.timestamp = [NSDate date];
        return [self saveContextWithError:error];
    }
    
    // Check if count overflows, if yes remove oldest item.
    NSUInteger count = [self getRecentSearchCount:error];
    if (count == NSNotFound) return NO;
    if (count>=self.maxSearchesInRecent && ![self deleteOldestSearch:error]) return NO;
    
    // Save the search item.
    NSString * entityName = NSStringFromClass([RecentSearch class]);
    RecentSearch * recentSearch = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                                inManagedObjectContext:self.managedObjectContext];
    recentSearch.queryString = searchString;
    recentSearch.timestamp = [NSDate date];
    
    return [self saveContextWithError:error];
    
}


@end
