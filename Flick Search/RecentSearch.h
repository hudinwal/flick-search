//
//  RecentSearch.h
//  Flick Search
//
//  Created by Dinesh on 26/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RecentSearch : NSManagedObject

@property (nonatomic, retain) NSString * queryString;
@property (nonatomic, retain) NSDate * timestamp;

@end
