//
//  FSMovieDetailViewController.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSMovie.h"

@interface FSMovieDetailViewController : UIViewController

@property (nonatomic,strong) FSMovie * movie;

@end
