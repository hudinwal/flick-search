//
//  NSString+FSStringChecks.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FSStringChecks)

/**
 Checks for string that has non repeating spaces.
 @return YES if string contains characters, otherwise NO.
 @warning Use cautiously, execution time is lot more than
 stringIsNotEmpty
 */
- (BOOL)stringHasCharacters;

/**
 Checks for string that has some characters, does not checks
 for all spaces
 
 @return YES if string contains some characters else NO
 
 */
- (BOOL)stringIsNotEmpty;

/**
 Helper method for getting the directory path or a file path
 inside a directoryfor given search path in current user domain.
 
 @note subPath parameter should start with "/" and must conatin
 the file extension in case of a file.
 
 @param searchPath NSSearchPathDirectory path to search
 @param subPath Optional argument to get specific directory
 path for given subPath.
 @return String path of the directory requested
 */
+ (instancetype)stringPathForDirectory:(NSSearchPathDirectory)searchPath
                           withSubPath:(NSString *)subPath;

@end
