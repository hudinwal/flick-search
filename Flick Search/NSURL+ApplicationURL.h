//
//  NSURL+ApplicationURL.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (ApplicationURL)

+(instancetype) urlToSearchMoviesForQuery:(NSString *)query
                             atPageNumber:(NSUInteger)page
                           withItemsLimit:(NSUInteger)limit;

+(instancetype) urlToFetchMovieDetailsForMovieID:(NSString *)movieID;

@end
