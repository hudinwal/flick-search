//
//  FSNetworkActivity.m
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSNetworkActivity.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

#define kActivityCountKey @"ActivityCount"

@interface FSNetworkActivity()

@property(nonatomic,assign) NSUInteger activityCount;

@end

@implementation FSNetworkActivity

static FSNetworkActivity * sharedInstance = nil;

+ (instancetype)sharedInstance
{
    static dispatch_once_t pred;
    
    if (nil != sharedInstance)
        return sharedInstance;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[FSNetworkActivity alloc] init];
    });
    
    return sharedInstance;
}

+(void)start
{
    [FSNetworkActivity sharedInstance].activityCount++;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

+(void)finished
{
    [FSNetworkActivity sharedInstance].activityCount--;
    if([FSNetworkActivity sharedInstance].activityCount == 0)
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
