//
//  FSLoadMoreView.m
//  Flick Search
//
//  Created by Dinesh on 23/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import "FSLoadMoreView.h"

@interface FSLoadMoreView()
{
    IBOutletCollection(UIImageView) NSArray * reelImageViews;
}

@property (nonatomic,assign) BOOL isAnimating;

@end

@implementation FSLoadMoreView

-(void)didMoveToSuperview
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restoreAnimation) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)restoreAnimation
{
    if(_isAnimating)
        [self startAnimating];
}

-(void)startAnimating
{
    _isAnimating = YES;
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0 * 2 * 60.0];
    rotationAnimation.duration = 80.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = INFINITY;
    
    for (int i=0; i<2; i++) {
        UIImageView * reel = reelImageViews[i];
        rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * (i+1) * 2 * 60.0];
        [reel.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    }
}

-(void)stopAnimating
{
    for (UIImageView * reel in reelImageViews)
        [reel.layer removeAnimationForKey:@"rotationAnimation"];
    _isAnimating = NO;
}

@end
