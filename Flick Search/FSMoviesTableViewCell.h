//
//  FSMoviesTableViewCell.h
//  Flick Search
//
//  Created by Dinesh on 24/01/15.
//  Copyright (c) 2015 Assignment. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSMovie.h"

@interface FSMoviesTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView * thumbnailView;

-(void)setUpCellFormMovie:(FSMovie *)movie;

-(void)setUpCellForRecentQueryString:(NSString *)query
                          itemNumber:(NSUInteger)number;

@end
